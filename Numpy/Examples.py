import numpy as np

print("0D")
#Matriz de 0 dimensiones
arr = np.array(42)
print(arr.ndim)
print(arr)

print("1D")
#Matriz de 1 Dimension
arr = np.array( [1,2,3,4,5] )
print(arr.ndim)
print(arr)

print("2D")
#Matriz de 2 Dimensiones
arr = np.array( [[1,2,3,4,5], [1,2,3,4,5]] )
print(arr.ndim)
print(arr)

print("3D")
#Matriz de 3 dimensiones
arr = np.array( [[[1,2,3,4,5], [1,2,3,4,5]], [[1,2,3,4,5], [1,2,3,4,5]]] )
print(arr.ndim)
print(arr)


arr = np.array([1,2,3,4,5], ndmin=6)
print(arr.ndim)
print(arr)