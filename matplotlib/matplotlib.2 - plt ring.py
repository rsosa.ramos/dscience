import matplotlib.pyplot as plt
import numpy as np

xpoints = np.array([1, 8, 16, 32, 48])
ypoints = np.array([3, 10, 2, 15, 19])

plt.plot(xpoints, ypoints, 'o')
plt.show()