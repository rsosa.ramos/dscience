import json

# a Python object (dict):
x = {
  "name": "John",
  "city": "New York"
}

# convert into JSON:
y = json.dumps(x)

# the result is a JSON string:
print(y)
