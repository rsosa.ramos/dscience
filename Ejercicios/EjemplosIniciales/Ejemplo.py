import calculadora as c

while True:
    c.menu()
    opcion = int(input("Seleccione una opcion: "))

    if opcion > 0 and opcion < 5:
        x = int(input("Digite el primer valor: "))
        y = int(input("Digite el segundo valor: "))

        if opcion == 1:
            print("RESULTADO: ")
            print(c.suma(x, y))
        elif opcion == 2:
            print("RESULTADO: ")
            print(c.resta(x, y))
        elif opcion == 3:
            print("RESULTADO: ")
            print(c.multiplicacion(x, y))
        else:
            print("RESULTADO: ")
            print(c.division(x, y))
    elif opcion == 5:
        print("Saliendo del programa")
        break
    else: 
        print("Opcion no valida")
