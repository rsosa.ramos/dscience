import datetime as d

x = d.datetime.now()
print(dir(d))
print(x.year)
print(x.strftime("%A"))

x = d.datetime(2020, 1, 17)
print(x.year)
print(x.month)
print(x.strftime("%B"))
print(x.strftime("%b"))
