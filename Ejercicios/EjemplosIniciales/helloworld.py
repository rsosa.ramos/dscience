print("hola mundo!")

x = str(3) #String
y = int(3) #Entero
z = float(3) #Float

"""
Comentarios
"""

print(type(x))
print(type(y))
print(type(z))

x = 4
y = 5

print(x)
print(y)

z = x + y
print("Suma: ", z)

z = x - y
print("Resta: ", z)

z = x * y
print("Multi: ", z)

z = x / y
print("Div: ", z)

z = x ** y
print("Potencia: ", z)

z = x // y
print("Div de Piso: ", z)

print("#################################################################################")
a = 200
b = 33
c = 500

if b > a:
    print("b es mas grande que a")

elif a == b:
        print("a y b son iguales ")

else:
    print("a es mayor a b")

print("Programa finalizado")
print(type(b > a))
print(b > a)



if (a > b or a > c):
    print("Al menos una es verdadera")