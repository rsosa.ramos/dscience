#Importamos la conexion con MongoDB
import conx.ConnAtlas as net
from bson.objectid import ObjectId


#print(net.client.test)
def getConex():
    evaluacion1db = net.client["Evaluacion01"]
    ceCollection = evaluacion1db["ce"]
    return ceCollection

def saveCe(nombre, departamento, municipio):
    ceDict = {"nombre": nombre, "departamento": departamento, "municipio": municipio}
    
    collection = getConex()
    
    x = collection.insert_one(ceDict)

    responsedict = {
                "codigo": "201",
                "mensaje": "El " + nombre + " ha sido guardado exitosamente con id " + str(x.inserted_id)
            }
    return responsedict


def findAllCe():
    collection = getConex()

    x = collection.find()

    return x

def updateCeById(id, nombre, departamento, municipio):
    collection = getConex()

    present_data = collection.find_one({"_id": ObjectId(id)})
    if nombre == "":
        nombre = present_data["nombre"]
    if departamento == "":
        departamento = present_data["departamento"]
    if municipio == "":
        municipio = present_data["municipio"]
        
    new_data = {'$set': {"nombre": nombre, "departamento": departamento, "municipio": municipio}}

    x = collection.update_one(present_data, new_data)

    if x.modified_count == 1:
        responsedict = {
                "codigo": "200",
                "mensaje": "El Centro Escolar ha sido modificado exitosamente con id " + id
            }
    else:
        responsedict = {
                "codigo": "204",
                "mensaje": "No se ha actualizado ningun registro con id " + id
            }

    return responsedict

def deleteCeById(id):
    collection = getConex()

    x = collection.delete_one({"_id":ObjectId(id)})

    if x.deleted_count == 1:
        responsedict = {
                "codigo": "200",
                "mensaje": "El Centro Escolar ha sido eliminado exitosamente con id " + id
            }
    else:
        responsedict = {
                "codigo": "204",
                "mensaje": "No se ha eliminado ningun registro con id " + id
            }

    return responsedict


#ret = saveCe("Centro Escolar Republica de Uruguay", "San Salvador", "Mejicanos")

#ret = findAllCe()
#for y in ret:
#    print("#####")
#    print(y) 

#ret = updateCeById("610cdc2f6eb30cce9743bc62", "Centro Escolar Republica Oriental del Japón", "San Salvador", "Mejicanos")
#print(ret)

#ret = deleteCeById("610cdc2f6eb30cce9743bc62")
#print(ret)