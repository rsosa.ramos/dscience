import proc.ProcesosCE as process
import os

while True:
    os.system("clear")
    print("MENU:")
    print("1. Agregar un Centro Escolar")
    print("2. Ver todos los Centros Escolares")
    print("3. Actualizar un Centro Escolar")
    print("4. Borrar un Centro Escolar por ID")
    print("5. Salir")

    opc = input("Ingrese su opción: ")

    if opc == "1":
        print("Ingrese los datos del Centro Escolar")
        nombre = input("Nombre: ")
        municipio = input("Municipio: ")
        departamento = input("Departamento:")

        x = process.saveCe(nombre, departamento, municipio)
        print(x["mensaje"])
        input()
    elif opc == "2":
        print("Los Centros Escolares guardados:")
        x = process.findAllCe()
        for y in x:
            print("------------------------")
            for value in y:
                print(value + ": " + str(y[value]))
        print("------------------------")
        input()
    elif opc == "3":
        print("Ingrese los datos del Centro Escolar a Actualizar:")
        print("Nota: Dejar vacios los campos que no desea Actualizar, el campo ID es obligatorio")
        
        id = input("ID: ")
        nombre = input("Nombre: ")
        municipio = input("Municipio: ")
        departamento = input("Departamento:")

        x = process.updateCeById(id, nombre, departamento, municipio)
        print(x["mensaje"])
        input()
    elif opc == "4":
        print("Ingrese el ID del Centro Escolar a Borrar:")
        id = input("ID: ")

        x = process.deleteCeById(id)
        print(x["mensaje"])

        input()
    elif opc == "5":
        print("Saliendo del Sistema...")
        print("Bye...")
        break
    else:
        print("Opción incorrecta - Presione enter para mostrar el Menú")
        input()
        continue