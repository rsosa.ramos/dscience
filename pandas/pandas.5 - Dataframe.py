import pandas as pd

data = {
  "calories": [420, 380, 390],
  "duration": [50, 40, 45]
}

#load data into a DataFrame object:
df = pd.DataFrame(data, index=["dia 1", "dia 2", "dia 3"])

print(df) 
print("########")
print(df.loc["dia 1"]) 