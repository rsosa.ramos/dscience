import numpy as np
import pandas as pd

# Creating the array to convert
numpy_array = np.array([[1, 'yo'], [4, 'bro'], [4, 'low'], [1, 'NumPy']])

# Create the dataframe
df = pd.DataFrame(numpy_array, columns=['digits', 'words'], index=["a", "b", "c", "d"])

print(df)